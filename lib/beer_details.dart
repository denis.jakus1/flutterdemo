import 'package:flutter/material.dart';
import 'feed.dart';

class BeerDetailsWidget extends StatelessWidget {

  final Beer beer;

  const BeerDetailsWidget({
    required this.beer,
  }): super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(beer.name),),
      body: Container(
        child: Column(
          children: [
            Container(
              child: Image.network(beer.imageUrl),
              height: 100,
              width: double.infinity,
              margin: EdgeInsets.all(20),
            ),
            Container(
              child: Text(beer.name, textAlign: TextAlign.center,),
              margin: EdgeInsets.all(20),
            ),
            Container(
              child: Text(beer.description, textAlign: TextAlign.center,),
              margin: EdgeInsets.all(20),
            )
          ],
        ),
      ),
    );
  }

}
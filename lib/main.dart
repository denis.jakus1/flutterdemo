import 'package:flutter/material.dart';
import 'package:flutter_course/calendar.dart';
import 'package:flutter_course/chat.dart';
import 'package:flutter_course/feed.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {

  var _title = "Beers";
  var _widgets = [FeedWidget(), CalendarWidget(), ChatWidget()];
  var _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(_title)),
      body: IndexedStack(
        index: _selectedIndex,
        children: _widgets,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex, // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            label: 'Feed',
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.calendar_today),
            label: 'Calendar',
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.chat),
              label: 'Chat'
          )
        ],
        onTap: (int index) {
          setState(() {
            _selectedIndex = index;
            if (index == 0) {
              _title = "Beer";
            } else if (index == 1) {
              _title = "Calendar";
            } else {
              _title = "Chat";
            }
          });
        },
      ),
    );
  }

}

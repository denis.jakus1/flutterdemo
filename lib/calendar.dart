import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CalendarState();
  }
}

class CalendarState extends State<CalendarWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TableCalendar(
            focusedDay: DateTime.now(),
            firstDay: DateTime.utc(2010, 10, 16),
            lastDay: DateTime.utc(2030, 3, 14),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          addEvent(context);
        },
        child: Icon(Icons.add),
      ),
    );
  }

  void addEvent(BuildContext context) {
    showModalBottomSheet(context: context, builder: (bCtx) {
      return AddEventModal();
    });
  }

}

class AddEventModal extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddEventModalState();
  }
}

class AddEventModalState extends State<AddEventModal> {

  var titleController = TextEditingController();
  var descriptionController = TextEditingController();
  var selectedDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: TextField(
            controller: titleController,
            decoration: InputDecoration(hintText: "Title"),
          ),
          margin: EdgeInsets.all(20),
        ),
        Container(
          child: TextField(
            controller: descriptionController,
            decoration: InputDecoration(hintText: "Description"),
          ),
          margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
        ),
        Container(
          alignment: Alignment.centerLeft,
          child:
            MaterialButton(
              child: Text(selectedDate.toString()),
              onPressed: () {
                var future = showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime.utc(2010, 10, 16), lastDate: DateTime.utc(2030, 10, 16));
                future.then((value) {
                  if (value != null) {
                    setState(() {
                      selectedDate = value;
                    });
                  }
                });
              },
            ),
          margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
        ),
        Container(
          alignment: Alignment.bottomRight,
          child:
          MaterialButton(
            child: Text("Add Event"),
            onPressed: () {

            },
          ),
          margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
        ),
      ],
    );
  }
}
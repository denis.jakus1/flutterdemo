import 'package:flutter/material.dart';
import 'package:flutter_course/beer_details.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class FeedWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BeerState();
  }
}

class _BeerState extends State<FeedWidget> {

  late Future<List<Beer>> futureBeers;

  @override
  void initState() {
    super.initState();
    futureBeers = fetchFeed();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Beer>>(
      future: futureBeers,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return getBeersListWidget(snapshot.requireData);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner.
        return Container(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  Future<List<Beer>> fetchFeed() async {
    final response = await http.get(Uri.parse('https://api.punkapi.com/v2/beers'));

    if (response.statusCode == 200) {
      return Beer.map(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load beers');
    }
  }

  Widget getBeersListWidget(List<Beer> beers) {
    List<Widget> items = [];
    for (var beer in beers) {
      items.add(
        ListTile(
          title: Text(beer.name),
          subtitle: Text(beer.description, maxLines: 2,),
          leading: Container(
            child: Image.network(beer.imageUrl),
            width: 40,
            height: double.infinity,
            alignment: Alignment.center,
          ),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (_) {
              return BeerDetailsWidget(beer: beer);
            }));
          },
        )
      );
    }

    return Container(
      child: ListView.separated(
          itemBuilder: (context, index) {
            return items[index];
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemCount: items.length
        ),
      margin: EdgeInsets.only(top: 5, bottom: 5),
      );
  }

}

class Beer {
  final int id;
  final String name;
  final String description;
  final String imageUrl;

  Beer({
    required this.id,
    required this.name,
    required this.description,
    required this.imageUrl
  });

  static List<Beer> map(List<dynamic> json) {
    List<Beer> beers = [];
    for (var item in json) {
      beers.add(Beer(
        id: item['id'],
        name: item['name'],
        description: item['description'],
        imageUrl: item['image_url'],
      ));
    }

    return beers;
  }
}
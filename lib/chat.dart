
import 'package:flutter/material.dart';

class ChatWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _ChatState();
  }

}

class _ChatState extends State<ChatWidget> {

  List<Message> messages = [OthersMessage("Hello, this is a demo app")];
  var textController = TextEditingController();
  var scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(child:
          ListView(
            controller: scrollController,
            children: getListItems(),
          ),
        ),
        Container(child:
          Row(
            children: [
              Expanded(child: TextField(
                controller: textController,
                decoration: InputDecoration(hintText: "Write a message..."),
              )),
              IconButton(icon: Icon(Icons.send, color: Colors.blue), onPressed: () {
                if (textController.text.isNotEmpty) {
                  setState(() {
                    messages.add(MyMessage(textController.text));
                    textController.text = "";
                    scrollController.animateTo(scrollController.position.maxScrollExtent, duration:
                    const Duration(milliseconds: 100), curve: Curves.easeOut);
                  });
                }
              },)
            ],
          ),
          margin: EdgeInsets.all(20),
        )
      ],
    );
  }

  List<Widget> getListItems() {
    List<Widget> items = [];
    for (var message in messages) {
      if (message is OthersMessage) {
        items.add(Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: Container(
                    padding: EdgeInsets.all(15),
                    margin: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(8.0))
                    ),
                    child: Text(message.message, style: TextStyle(color: Colors.white)),
                  ),
                ),
              ],
            ),
          ],
        ));
      } else if (message is MyMessage) {
        items.add(Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: Container(
                    padding: EdgeInsets.all(15),
                    margin: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(8.0))
                    ),
                    child: Text(message.message, style: TextStyle(color: Colors.white)),
                  ),
                ),
              ],
            )
          ],
        ));
      }
    }
    return items;
  }

}

class Message {
  String message = "";
  Message(this.message);
}

class MyMessage extends Message {
  MyMessage(String message) : super(message);
}

class OthersMessage extends Message {
  OthersMessage(String message) : super(message);
}